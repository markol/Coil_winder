# Coil winder specification

# in milimeters
workAreaMaxLength = 150
# revolutions / minute
maxSpeed = 500
maxAcceleration = 500
# baud rates avaiable in dropdown list
bauds=("9600", "115200")