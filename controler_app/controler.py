#!/usr/bin/env python
# -*- coding: utf-8 -*-
import wx, time
from control_serial import *
import config

class AppMainPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        
        self.parent = parent
        
        # Config variables
        self.workAreaMaxLength = config.workAreaMaxLength 
        
        # State variables
        self.rightMargin = 0
        self.leftMargin = self.workAreaMaxLength
        self.setRightMargin = self.rightMargin
        self.setLeftMargin = self.leftMargin
        self.workAreaLength = self.leftMargin - self.rightMargin
        self.currentPos = self.rightMargin
        self.posMsg = "Unknown"
        
        # wx timer
        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.OnTimerUpdate, self.timer)
        
        # Buttons
        self.leftGoToBtn = wx.Button(self, label="Go to")
        self.rightGoToBtn = wx.Button(self, label="Go to")
        self.connectBtn = wx.Button(self, label="Connect")
        self.homeBtn = wx.Button(self, label="Home")
        self.setMarginsBtn = wx.Button(self, label="Set margins")
        self.setStartPosBtn = wx.Button(self, label="Get starting pos")
        self.setSpeedBtn = wx.Button(self, label="Set movement")
        self.queueBtn = wx.Button(self, label="Queue command")
        self.clearBtn = wx.Button(self, label="Clear queue")
        self.executeBtn = wx.Button(self, label="Execute queue")
        self.stopBtn = wx.Button(self, label="Stop")
        
        self.Bind(wx.EVT_BUTTON, self.OnClickGotoL,self.leftGoToBtn)
        self.Bind(wx.EVT_BUTTON, self.OnClickGotoR,self.rightGoToBtn)
        self.Bind(wx.EVT_BUTTON, self.OnClickConnect,self.connectBtn)
        self.Bind(wx.EVT_BUTTON, self.OnClickHome,self.homeBtn)
        self.Bind(wx.EVT_BUTTON, self.OnClickSetMargins,self.setMarginsBtn)
        self.Bind(wx.EVT_BUTTON, self.OnClickSetStartPos,self.setStartPosBtn)
        self.Bind(wx.EVT_BUTTON, self.OnClickSetSpeed,self.setSpeedBtn)
        self.Bind(wx.EVT_BUTTON, self.OnClickClear,self.clearBtn)
        self.Bind(wx.EVT_BUTTON, self.OnClickQueue,self.queueBtn)
        self.Bind(wx.EVT_BUTTON, self.OnClickExecute,self.executeBtn)
        self.Bind(wx.EVT_BUTTON, self.OnClickStop,self.stopBtn)
  
        # Sliders
        self.leftMarginSlider = wx.Slider(self, value=(self.workAreaMaxLength-self.leftMargin)*1000, minValue=0, maxValue=self.workAreaMaxLength*1000, style=wx.SL_HORIZONTAL)
        self.rightMarginSlider = wx.Slider(self, value=(self.workAreaMaxLength-self.rightMargin)*1000, minValue=0, maxValue=self.workAreaMaxLength*1000, style=wx.SL_HORIZONTAL)
        self.leftMarginSlider.Bind(wx.EVT_SCROLL, self.OnScrollLeftMargin)
        self.rightMarginSlider.Bind(wx.EVT_SCROLL, self.OnScrollRightMargin)
        
        # Controls
        self.wireDiaText = wx.TextCtrl(self, value="0.3", size=(55,-1))
        self.windingsAmoutText = wx.TextCtrl(self, value="100", size=(100,-1))
        self.speedText = wx.TextCtrl(self, value=str(config.maxSpeed/2), size=(50,-1))
        self.accelerationText = wx.TextCtrl(self, value=str(config.maxAcceleration/2), size=(50,-1))
        
        self.wireDiaText.Bind(wx.EVT_CHAR, self.OnCharRestrict) 
        self.windingsAmoutText.Bind(wx.EVT_CHAR, self.OnCharRestrict) 
        self.speedText.Bind(wx.EVT_CHAR, self.OnCharRestrict) 
        self.accelerationText.Bind(wx.EVT_CHAR, self.OnCharRestrict) 
        
        # Combo ports
        self.portsCombo = wx.ComboBox(self, choices=serial_ports())
        if (not self.portsCombo.IsListEmpty()):
            self.portsCombo.SetSelection(0)
        self.baudrateCombo = wx.ComboBox(self, choices=config.bauds)
        self.baudrateCombo.SetSelection(0)
        
        # Labels
        self.rightPos = wx.StaticText(self, label=str(round(self.rightMargin,2)), size=(45, -1))
        self.leftPos = wx.StaticText(self, label=str(round(self.leftMargin,2)), size=(45, -1))
        
        # Gcode preview
        self.queueEdit = wx.TextCtrl(self,  style=wx.TE_MULTILINE | wx.TE_READONLY)
        self.loggerEdit = wx.TextCtrl(self, style=wx.TE_MULTILINE | wx.TE_READONLY)
        
        # Radio's
        self.guideDirRadio = wx.RadioBox(
                self, wx.ID_ANY, "Guide direction", wx.DefaultPosition,
                wx.DefaultSize, ("Left", "Right"), 2, wx.RA_SPECIFY_COLS
                )
        self.windingDirRadio = wx.RadioBox(
                self, wx.ID_ANY, "Winding direction", wx.DefaultPosition,
                wx.DefaultSize, ("Upwards", "Downward"), 2, wx.RA_SPECIFY_COLS
                )
        
        # Sizers
        mainSizer = wx.BoxSizer(wx.HORIZONTAL)
        topSizer = wx.BoxSizer(wx.VERTICAL)
        
        connectionSizer = wx.BoxSizer(wx.HORIZONTAL)
        leftHSizer = wx.BoxSizer(wx.HORIZONTAL)
        rightHSizer = wx.BoxSizer(wx.HORIZONTAL)
        
        # Layout
        connectionSizer.Add(wx.StaticText(self, label='Port:'), 0, wx.ALIGN_CENTER_VERTICAL | wx.FIXED_MINSIZE, wx.LEFT)
        connectionSizer.Add(self.portsCombo, 0, wx.ALIGN_CENTER_VERTICAL | wx.FIXED_MINSIZE, wx.LEFT)
        connectionSizer.AddSpacer(10)
        connectionSizer.Add(wx.StaticText(self, label='Baudrate:'), 0, wx.ALIGN_CENTER_VERTICAL | wx.FIXED_MINSIZE, wx.LEFT)
        connectionSizer.Add(self.baudrateCombo, 0, wx.ALIGN_CENTER_VERTICAL | wx.FIXED_MINSIZE, wx.LEFT)
        connectionSizer.AddSpacer(20)
        connectionSizer.Add(self.connectBtn, 0, wx.ALIGN_CENTER_VERTICAL | wx.FIXED_MINSIZE, wx.LEFT)
        
        rightHSizer.Add(self.rightMarginSlider, 2, wx.EXPAND, wx.ALL)
        rightHSizer.Add(self.rightPos, 0, wx.ALIGN_CENTER_VERTICAL | wx.FIXED_MINSIZE, wx.LEFT)
        rightHSizer.Add(self.rightGoToBtn, 0, wx.ALIGN_CENTER_VERTICAL | wx.FIXED_MINSIZE, wx.LEFT)
        
        leftHSizer.Add(self.leftMarginSlider, 2, wx.EXPAND, wx.ALL)
        leftHSizer.Add(self.leftPos, 0, wx.ALIGN_CENTER_VERTICAL | wx.FIXED_MINSIZE, wx.LEFT)
        leftHSizer.Add(self.leftGoToBtn, 0, wx.ALIGN_CENTER_VERTICAL | wx.FIXED_MINSIZE, wx.LEFT)
        
        topSizer.Add(connectionSizer, 1, wx.ALIGN_CENTER_HORIZONTAL)
        topSizer.AddSpacer(20)
        topSizer.Add(wx.StaticText(self, label='Right margin:'), 0, wx.FIXED_MINSIZE, wx.LEFT)
        topSizer.Add(rightHSizer, 1, wx.EXPAND, wx.ALL)
        topSizer.AddSpacer(10)
        topSizer.Add(wx.StaticText(self, label='Left margin:'), 0, wx.FIXED_MINSIZE, wx.LEFT)
        topSizer.Add(leftHSizer, 1, wx.EXPAND, wx.ALL)
        
        workAreaSizer = wx.BoxSizer(wx.HORIZONTAL)
        workAreaSizer.Add(self.setMarginsBtn, 1,  wx.LEFT)
        workAreaSizer.Add(self.homeBtn, 1,  wx.RIGHT)
        topSizer.Add(workAreaSizer, 1, wx.EXPAND, wx.ALL)
        topSizer.AddSpacer(20)
        dirParamsSizer = wx.BoxSizer(wx.HORIZONTAL)
        dirParamsSizer.Add(self.guideDirRadio, 1, wx.FIXED_MINSIZE | wx.ALIGN_CENTER, wx.LEFT)
        dirParamsSizer.Add(self.windingDirRadio, 1, wx.FIXED_MINSIZE | wx.ALIGN_CENTER, wx.LEFT)
        topSizer.Add(dirParamsSizer, 1, wx.EXPAND, wx.ALL)
        topSizer.AddSpacer(10)
        speedParamsSizer = wx.BoxSizer(wx.HORIZONTAL)
        speedParamsSizer.Add(wx.StaticText(self, label='Speed:'), 0, wx.FIXED_MINSIZE | wx.ALIGN_CENTER_VERTICAL, wx.LEFT)
        speedParamsSizer.Add(self.speedText, 0, wx.ALIGN_CENTER_VERTICAL, wx.LEFT)
        speedParamsSizer.AddSpacer(10)
        speedParamsSizer.Add(wx.StaticText(self, label='Acceleration:'), 0, wx.FIXED_MINSIZE | wx.ALIGN_CENTER_VERTICAL, wx.LEFT)
        speedParamsSizer.Add(self.accelerationText, 1, wx.FIXED_MINSIZE | wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL, wx.LEFT)
        speedParamsSizer.Add(self.setStartPosBtn, 1, wx.FIXED_MINSIZE | wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL, wx.LEFT)
        speedParamsSizer.Add(self.setSpeedBtn, 1, wx.FIXED_MINSIZE | wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL, wx.LEFT)
        topSizer.Add(speedParamsSizer, 1, wx.EXPAND, wx.ALL)
        topSizer.AddSpacer(10)
        paramsSizer = wx.BoxSizer(wx.HORIZONTAL)
        paramsSizer.Add(wx.StaticText(self, label='Wire diameter:'), 0, wx.ALIGN_CENTER_VERTICAL | wx.FIXED_MINSIZE, wx.LEFT)
        paramsSizer.Add(self.wireDiaText, 0,  wx.ALIGN_CENTER_VERTICAL, wx.LEFT)
        paramsSizer.AddSpacer(10)
        paramsSizer.Add(wx.StaticText(self, label='Windings count:'), 0, wx.ALIGN_CENTER_VERTICAL | wx.FIXED_MINSIZE, wx.LEFT)
        paramsSizer.Add(self.windingsAmoutText, 1, wx.FIXED_MINSIZE | wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL, wx.LEFT)
        paramsSizer.Add(self.queueBtn, 1, wx.FIXED_MINSIZE | wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL, wx.LEFT)
        topSizer.Add(paramsSizer, 1, wx.EXPAND, wx.ALL )
        
        controlSizer = wx.BoxSizer(wx.HORIZONTAL)
        controlSizer.Add(self.clearBtn, 1, wx.FIXED_MINSIZE | wx.ALIGN_LEFT, wx.LEFT)
        controlSizer.AddSpacer(30)
        controlSizer.Add(self.stopBtn, 1, wx.FIXED_MINSIZE | wx.ALIGN_LEFT, wx.LEFT)
        controlSizer.AddSpacer(30)
        controlSizer.Add(self.executeBtn, 1, wx.FIXED_MINSIZE | wx.ALIGN_LEFT, wx.LEFT)
        topSizer.Add(controlSizer, 1, wx.FIXED_MINSIZE | wx.ALIGN_CENTER_HORIZONTAL, wx.LEFT)
        
        # Jogging controls
        joggSizer = wx.GridSizer(3, 3, 1, 1)
        up = wx.Button(self, label='/\\')
        down = wx.Button(self, label='\\/')
        left = wx.Button(self, label='<')
        right = wx.Button(self, label='>')
        self.Bind(wx.EVT_BUTTON, lambda event: queue_command("G91\n") or queue_command("G1 X0.2\n"), up)
        self.Bind(wx.EVT_BUTTON, lambda event: queue_command("G91\n") or queue_command("G1 X-0.2\n"), down)
        self.Bind(wx.EVT_BUTTON, lambda event: queue_command("G91\n") or queue_command("G1 Y0.1\n"), left)
        self.Bind(wx.EVT_BUTTON, lambda event: queue_command("G91\n") or queue_command("G1 Y-0.1\n"), right)
        
        joggSizer.AddMany( [ 
            (wx.StaticText(self), wx.EXPAND),
            (up, 0, wx.EXPAND),
            (wx.StaticText(self), wx.EXPAND),
            (left, 0, wx.EXPAND),
            (wx.StaticText(self), wx.EXPAND),
            (right, 0, wx.EXPAND),
            (wx.StaticText(self), wx.EXPAND),
            (down, 0, wx.EXPAND),
            (wx.StaticText(self), wx.EXPAND),
            ])
        topSizer.Add(joggSizer, 1, wx.FIXED_MINSIZE | wx.ALIGN_CENTER_HORIZONTAL, wx.LEFT)
        
        mainSizer.Add(topSizer, 3, wx.ALL, 10)
        #mainSizer.AddSpacer(10)
        loggerSizer = wx.BoxSizer(wx.VERTICAL)
        loggerSizer.Add(self.loggerEdit, 1, wx.EXPAND, wx.ALL)
        loggerSizer.Add(self.queueEdit, 1, wx.EXPAND, wx.ALL)
        mainSizer.Add(loggerSizer, 1, wx.EXPAND, wx.ALL)
            
        self.SetSizer(mainSizer)
    
    # ------------------------------------------------------------------------
    
    def OnTimerUpdate(self, event):
        # Called every 1,5 sec when device is connected to update the displayed position
        # On M114 mcode device returns its position witch we want to print in status bar

        def assign(response):
            self.posMsg = response.splitlines()[0]
            
        queue_command("M114\n", assign, priority = -1)
        time.sleep(0.2)
        self.parent.SetStatusText(self.posMsg)
        
        
    def OnCharRestrict(self, event):
        # This method is called when character is typed and input is focused. characters are trimmed to digits and comma only.
        key = event.GetKeyCode()
        if key > 255:
            if key == 314 or key == 316: # allow 314 & 316 arrows, 
                event.Skip()
                return
        elif chr(key) == '.':
            if '.' in event.GetEventObject().GetValue():
                return False
            event.Skip()
            return
        elif chr(key) in "1234567890" or ( key > 0 and key < 31) or key == 127: # allow 8 backspace, 127 del, 13 enter
            event.Skip()
            return

        else:
            return False 
    
    # Those two methods are for setting a winding workarea margins
    def OnScrollLeftMargin(self,event):
        valL = abs(self.leftMarginSlider.GetValue() - self.leftMarginSlider.GetMax())
        valR = abs(self.rightMarginSlider.GetValue() - self.rightMarginSlider.GetMax())
        if valL < valR:
            self.leftMarginSlider.SetValue(self.rightMarginSlider.GetValue())
        self.leftMargin = valL/1000.0
        self.leftPos.SetLabel(str(round(self.leftMargin,2)))
    def OnScrollRightMargin(self,event):
        valL = abs(self.leftMarginSlider.GetValue() - self.leftMarginSlider.GetMax())
        valR = abs(self.rightMarginSlider.GetValue() - self.rightMarginSlider.GetMax())
        
        if valR > valL:
            self.rightMarginSlider.SetValue(self.leftMarginSlider.GetValue())
        self.rightMargin = valR/1000.0
        self.rightPos.SetLabel(str(round(self.rightMargin, 2)))

    # Control commands
    def OnClickConnect(self,event):
        if ser.isOpen():
            close_serial()
            self.timer.Stop()
            self.connectBtn.SetLabel("Connect")
            self.parent.SetStatusText("Not connected")
            return
        self.loggerEdit.AppendText("Connecting...\n")
        port = self.portsCombo.GetValue()
        baud = self.baudrateCombo.GetValue()
        onResult = lambda msg: self.loggerEdit.AppendText(msg)
        success = open_serial(port, baud, onResult)
        if success:
            self.connectBtn.SetLabel("Disconnect")
            self.timer.Start(1500)
            
        #self.loggerEdit.AppendText(" Click on object with Id %d\n" %event.GetId())
    def OnClickExecute(self,event):
        self.loggerEdit.AppendText("Starting planned queue\n")
        commands = self.queueEdit.GetValue().splitlines(True)
        queue_command("G91\n")
        queue_command_list(commands)
    def OnClickStop(self,event):
        queue.clear()
        queue_command("M0\n", priority = -2)
        self.loggerEdit.AppendText("Stopping\n")
    def OnClickGotoL(self,event):
        print ("l")
        queue_command("G90\n")
        queue_command("G1 Y%f\n" % self.leftMargin)
    def OnClickGotoR(self,event):
        print ("r")
        queue_command("G90\n")
        queue_command("G1 Y%f\n" % self.rightMargin)
    def OnClickHome(self,event):
        print ("home")
        queue_command("G28\n")
    def OnClickSetMargins(self,event):
        self.workAreaLength = (self.leftMargin - self.rightMargin) 
        self.setLeftMargin = self.leftMargin
        self.setRightMargin = self.rightMargin
        self.currentPos = self.rightMargin
        self.loggerEdit.AppendText("Setting work area length to %f\n" %self.workAreaLength)
    def OnClickSetStartPos(self,event):
        def setPos(msg):
            # get Y pos from pos string
            pos = float(msg.splitlines()[0].split(",")[1][2:-1])
            self.currentPos = pos
            if(self.currentPos < 0):
                self.currentPos = 0
            
        queue_command("M114\n", setPos)
    def OnClickSetSpeed(self,event):
        speed = float(self.speedText.GetValue())
        acceleration = float(self.accelerationText.GetValue())
        # check limits
        if speed > config.maxSpeed:
            speed = config.maxSpeed
            self.speedText.SetValue(str(speed))
        if acceleration > config.maxAcceleration:
            acceleration = config.maxAcceleration
            self.accelerationText.SetValue(str(acceleration))
        queue_command("M222 F%d\n" % speed, priority = -1)
        print ("speeds")
    def OnClickQueue(self,event):
        # This method generates gcode commands for winding machine from specified winding parameters
        totalTurns = int(self.windingsAmoutText.GetValue())
        wireDia = float(self.wireDiaText.GetValue())
        fullLayers = 0
        turnsLastLayer = 0
        totalLayers = 1
        # acts as multipler, values 1 or -1
        windindDirection = 1 if self.windingDirRadio.GetSelection() == 0 else -1
        guideDirection = 1 if self.guideDirRadio.GetSelection() == 0 else -1
        
        # Calculations
        turnsPerLayer = totalTurns
        turnsFirstLayer = totalTurns
        if wireDia:
            turnsPerLayer = int(self.workAreaLength / wireDia)
            turnsFirstLayer = int(( self.setLeftMargin-self.currentPos if guideDirection > 0 else self.currentPos-self.setRightMargin)/wireDia)
        
        # All the turns are fit on first layer
        if totalTurns < turnsFirstLayer:
            turnsFirstLayer = totalTurns
            
        # More layers needed
        elif totalTurns > turnsFirstLayer:
            totalTurnsWitoutFirstLayer = totalTurns - turnsFirstLayer
            fullLayers = int(totalTurnsWitoutFirstLayer/turnsPerLayer)
            turnsLastLayer = totalTurnsWitoutFirstLayer - fullLayers*turnsPerLayer
            if turnsLastLayer == turnsPerLayer: 
                turnsLastLayer = 0
         
         
        totalLayers += fullLayers
        if turnsLastLayer > 0:
            totalLayers += 1
        
        
        self.loggerEdit.AppendText("Queue turns: %d, per layer: %d, total layers: %d\n" %(totalTurns, turnsPerLayer, totalLayers))
        
        Y_dst = turnsFirstLayer*wireDia*guideDirection
        self.queueEdit.AppendText("G1 X%d Y%f\n" %(turnsFirstLayer*windindDirection, Y_dst))
        guideDirection *= -1
        self.currentPos += Y_dst
        
        turns = 0
        for layer in range(fullLayers):
            turns += turnsPerLayer
            # nonrelative coords
            Y_dst = turnsPerLayer*wireDia*guideDirection
            self.queueEdit.AppendText("G1 X%d Y%f\n" %(turnsPerLayer*windindDirection, Y_dst))
            guideDirection *= -1
            self.currentPos += Y_dst
        
        if turnsLastLayer > 0:
            Y_dst = turnsLastLayer*wireDia*guideDirection
            self.queueEdit.AppendText("G1 X%d Y%f\n" %(turnsLastLayer*windindDirection, Y_dst))
            guideDirection *= -1
            self.currentPos += Y_dst
        

        if guideDirection < 0:
            self.guideDirRadio.SetSelection(0)
        else:
            self.guideDirRadio.SetSelection(1)
            
        print(self.currentPos)
        
    def OnClickClear(self, event):
        self.queueEdit.Clear()
    
    def LoadQueueFile(self, path):
        with open(path, "r") as file:
             self.queueEdit.SetValue(file.read())
             file.close()
    def SaveQueueFile(self, path):
        with open(path, "w") as file:
             file.write(self.queueEdit.GetValue())
             file.close()

class AppMainFrame(wx.Frame):
  
    def __init__(self, parent):
        super(AppMainFrame, self).__init__(parent, title = "Winder control", size=(800,600))
        self.InitUI()
        
    def InitUI(self):
        self.menubar = wx.MenuBar()
        fileMenu = wx.Menu()
        opn = fileMenu.Append(wx.ID_OPEN, '&Open GCode queue')
        save = fileMenu.Append(wx.ID_SAVE, '&Save current queue')
        aboutMenu = wx.Menu()
        about = aboutMenu.Append(wx.ID_ABOUT, "&About", "Information about this program")
        aboutMenu.Bind(wx.EVT_MENU, self.OnAbout, about)
        fileMenu.Bind(wx.EVT_MENU, self.OnLoadFile, opn)
        fileMenu.Bind(wx.EVT_MENU, self.OnSaveFile, save)
        
        self.menubar.Append(fileMenu, '&File')
        self.menubar.Append(aboutMenu, '&About')
        self.SetMenuBar(self.menubar)
        self.CreateStatusBar()
        self.SetStatusText("Not connected")
    
    def SetPanel(self, panel):
        self.panel = panel
    def OnAbout(self, event):
        dlg = wx.MessageDialog(self, "Written in 2017", "Information about this software", wx.OK|wx.ICON_INFORMATION)
        result = dlg.ShowModal()
        dlg.Destroy() 
        
    def OnLoadFile(self, event):
        openFileDialog = wx.FileDialog(self, "Open", "", "", 
                                       "Text files (*.txt)|*.txt", 
                                       wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        openFileDialog.ShowModal()
        self.panel.LoadQueueFile(openFileDialog.GetPath())
        openFileDialog.Destroy()
    
    def OnSaveFile(self, event):
        saveFileDialog = wx.FileDialog(self, "Save As", "", "", 
                                       "Text files (*.txt)|*.txt", 
                                       wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
        saveFileDialog.ShowModal()
        self.panel.SaveQueueFile(saveFileDialog.GetPath())
        saveFileDialog.Destroy()
        

if __name__ == '__main__':
    app = wx.App(False)
    if int(wx.__version__[0]) < 3:
        dlg = wx.MessageDialog(None, "Your wxPython version is probably older than 3.0! \nPlease update.", "wx version mismatch", wx.OK | wx.ICON_ERROR)
        dlg.ShowModal()
    else:
        frame = AppMainFrame(None)
        panel = AppMainPanel(frame)
        frame.SetPanel(panel)
        frame.Center()
        frame.Show()
        app.MainLoop()
        if ser.isOpen():
            close_serial()
