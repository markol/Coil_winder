## This directory contains python scripts for coil winder controller GUI
Tested under python 3.10 with wxPython 3.0.5 Run main file `controller.py`.

Modify file `config.py` for your custom winding machine setup.

`sudo pip install --upgrade --pre -f https://wxpython.org/Phoenix/snapshot-builds/ wxPython` or `apt-get install python-wxgtk3.0`

## Usage
Connect with device on selected port and speed (baud).

Home wire guide carriage using *Home* button. Using sliders, set the working area, where wire is going to be wound. Position can be checked by clicking *Go to* button, which causes carriage to move into desired pos. Finally set margins by clicking *Set margins* button. 

Select the initial winding directions, defaults are fine. Chose proper speed for your job, according to wire diameter, by typing it in *Speed* field (in revolutions per minute). Send speed to machine by clicking *Set movement*. Set speed can be checked with jog buttons (up and down arrow buttons, */\*, *\/*). Type the wire diameter (with insulation) and amount of wanted windings into *Wire diameter* and *Windings count* fields respectively. *Queue command* will generate all the needed commands to control the machine, all queued instructions are visible in bottom text field. Note: positions in commands are relative, depending of work area and wire dia, all the needed layers are calculated. Sophisticated setups can by defined (example, higher spacing between each 10 windings), when queuing tasks multiple times with diffrent spacing. 

*Execute queue* starts the job defined in queue. *Stop* button stops the running machine, but not always immediately - if on-device buffer is full, wait for end of processing current command (layer moslty). Current status of the whole process is shown at bottom status bar.  X value means number of done windings, Y is carriage absolute position, F is current speed.

If you want to start winding from perviously emergency stopped work, use *Get starting pos* on connected machine, before queuing new commands. 

## List of used G-codes
- **G1 X? Y?** - X destination position in number of turns (1 unit is one one turn), Y carriage destination position in mm's.
- **G28** - home carriage
- **G90** - absolute positioning
- **G91** - relative positioning
- **M0** - stop or unconditional stop
- **M202 X? Y?** - set max acceleration
- **M222 X? Y? F?** - set speed of fast XY moves, F linear move of all axes (feedrate)
- **M119** - get endstop status
- **M114** - get current position

