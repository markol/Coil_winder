## To print
- 3x *bearing8mm\_distance.fcstd*
- 1x *carriage\_base.fcstd*
- 1x *carriage\_lid.fcstd*
- 1x *carriage\_motor\_holder.fcstd*
- 2x *coupler\_holder.fcstd*
- 2x *frame\_vertex\_bearings.fcstd*
- 1x *frame\_vertex\_bearings\_motor.fcstd*
- 3x *frame\_vertex\_foot2.fcstd*
- 3x *frame\_vertex\_foot.fcstd*
- 2x *knob\_tightener.fcstd*
- 2x *rod\_clamp.fcstd*
- 1x *wire\_guide2.fcstd*
- 1x *wire\_guide\_arm1.fcstd*
- 1x *wire\_guide\_arm2.fcstd*
- 1x *wire\_guide\_base.fcstd*
- 1x *wire\_guide.fcstd* - optional, another variant
- 3x *wire\_guide\_wheel.fcstd*
- 3x *wire\_guide\_wheel\_lid.fcstd*
- 2x *wire\_tensioner\_knob.fcstd*
- 2x *wire\_tensioner\_washer\_backplate.fcstd*
- 2x *wire\_tensioner\_washer.fcstd*


Check also files from scad directory for necessary parametric gears (*winder\_gears.scad*), pulleys (*winder\_pulleys.scad*) and tools to print.
To get newest STL's for 3D printing, open each needed file and export to STL. 

## Assemblies
- 1x *assembly\_carriage.fcstd*
- 2x *assembly\_triangle.fcstd*
- 1x *assembly\_triangle\_motor.fcstd*

## Non printable
- 6x 608ZZ\_Ball\_Bearing
- 2x 626ZZ\_Ball\_Bearing
- 2x 623ZZ\_Ball\_Bearing
- 3x 693ZZ\_Ball\_Bearing
- 76x DIN 125 class 4 M8 Flat Washer
- 4x ISO4032\_Hex\_Nut\_M6
- 76x ISO4032\_Hex\_Nut\_M8
- 3x ISO7091DIN126\_CLASS\_C\_M6
- 1x NEMA-17\_Stepper\_Motor\_40mm -> high torque
- 2x rod\_8x250mm
- 1x rod\_M6\_220mm
- 1x rod\_M8\_125mm
- 3x rod\_M8\_160mm
- 1x rod\_M8\_210mm
- 3x rod\_M8\_270mm
- 4x rod\_M8\_300mm
- 3x rod\_M8\_330mm
- 1x stepper\_tin (or other any torque NEMA-17)
- some M3 screws, nuts and washers