//Uncomment and export one of the solids bellow
//cone();
motorc();
//motorcc();

module motorcc()
{
	difference(){
	union(){
		cylinder(r = 17, h = 18);	
		translate([-10,-50,0])	cube([20,100,4]);
		//translate([-15,-45,2])	cube([30,90,3]);
	}
	for(d = [6+5*3:5:46])
	{	
		translate([0,d,0]) cylinder(r=4/2, h = 5);
		translate([0,-d,0]) cylinder(r=4/2, h = 5);
	}
	translate([0,0,18]) scale([1,1,-1]) coupler();
	}

}

module motorc()
{
	guide_external_dia = 30;
	giude_indentation_r = 15/2;
	width = guide_external_dia+ giude_indentation_r*2;
	mount_width = 20.3;
	mount_height = 3.2;
	mount_hole_dist = 6;

	rotate([-90,0,0])
	difference(){
	translate([0,0,-giude_indentation_r-1-mount_height])cylinder(r = width/2, h = giude_indentation_r*2+mount_height*2+2);
	
	translate([-width/2,0,-giude_indentation_r-1-mount_height]) cube([width,width/2,giude_indentation_r*2+mount_height*2+2]);
	
	rotate_extrude(convexity = 10)
		translate([guide_external_dia/2+giude_indentation_r, 0, 0])
		circle(r = giude_indentation_r, $fn = 10);

	//mounting holes
		translate([-mount_width/2,-width/2, giude_indentation_r+1]) cube([mount_width,width,mount_height]);
		translate([-mount_width/2,-width/2, -giude_indentation_r-1-mount_height]) cube([mount_width,width,mount_height]);
	translate([0,-mount_hole_dist, -giude_indentation_r-1]) cylinder(r=4/2, h = giude_indentation_r*2+5);
	}
}

module cone(){
mirror([0,0,1])
difference(){
cylinder(r1=30,r2=10,h=20);
coupler();
}
}

module coupler()
{
	r = 30.5/2;
translate([0,0,5])
union(){
difference()
{
	cylinder(r=r, h=6);

	cube([30,30,30]);
	mirror([1,1,0]) cube([30,30,30]);
	
}
//nut
cylinder(r=7.6, h=10, $fn=6);
cylinder(r=4.4, h=50);
}
cylinder(r=r, h=5);
}
