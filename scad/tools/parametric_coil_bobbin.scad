// section size of the transformer core (bobbin internal)
coreWidth = 22;
coreHight = 36.9;
// how deep (long)the core is, deepth of the bobbin (total, external)
// divided by half for two parts
coreDeepth = 48/2;
// width of the bobbin sectional walls (from inside of core hole)
coilThickness = 8;

// thicknes of the coil core walls
coreShellThickness = 1.6;
// thickness of the bobbin sectional walls
wallThickness = 2;

// rounding of the sectional walls corners, set to small value for squared
roundRadius = 8;
// additional width for the connection terminals in sectional walls, set 0 for none
distanceForTerminals = 0;

// glue together after printing
bobbin();
translate([0,coreWidth+coilThickness*2+2,0]) bobbin(true);


module bobbin(terminalHole = false, terminalsEnforce = 1, rivetsPocket = false)
{
difference()
{
	union()
	{
		cube([coreHight+coreShellThickness*2 , coreWidth+coreShellThickness*2 , coreDeepth ], center=true);

		if(terminalsEnforce == 2)
			translate([0,0,-coreDeepth/2+wallThickness/2])
			roundedcube(2*distanceForTerminals+coreHight+coilThickness*2,coreWidth+coilThickness*2,
			wallThickness ,roundRadius);
		if(terminalsEnforce == 1)
			translate([distanceForTerminals/2,0,-coreDeepth/2+wallThickness/2])
			roundedcube(distanceForTerminals+coreHight+coilThickness*2,coreWidth+coilThickness*2,
			wallThickness ,roundRadius);

	}

	//main core hole
	cube([coreHight, coreWidth, coreDeepth+10], center=true);
	if(rivetsPocket)
		cube([coreHight+0.6, 8, coreDeepth+10], center=true);

	//terminals holes
	hole_r = 1.2;
	if(terminalHole)
	translate([coreHight/2+coreShellThickness+hole_r-0.3, coreWidth/2+coreShellThickness,-coreDeepth/2])
		cylinder(r=hole_r, h=wallThickness+1, $fn =7);
}
}
module roundedcube(xdim ,ydim ,zdim,rdim)
{
	translate([-xdim/2,-ydim/2, -zdim/2])
	{
		hull()
		{
			translate([rdim,rdim,0]) cylinder(r=rdim, h=zdim);
			translate([xdim-rdim,rdim,0])cylinder(r=rdim, h=zdim);
	
			translate([rdim,ydim-rdim,0])cylinder(r=rdim, h=zdim);
			translate([xdim-rdim,ydim-rdim,0]) cylinder(r=rdim, h=zdim);
		}
	}
}