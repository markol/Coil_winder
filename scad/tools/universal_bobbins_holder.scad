
difference(){
	coupler(10.5);

translate([-8,8,15]) rotate([0,90,0]) screw();
translate([5,8,35]) rotate([0,90,0]) screw();
translate([5,-8,15]) rotate([180,-90,0]) screw();
translate([-8,-8,35]) rotate([180,-90,0]) screw();

translate([-9,-5,28]) rotate([0,-90,90]) screw();
translate([9,8,28]) rotate([180,90,90]) screw();
}

module coupler(r = 26/2)
{
	difference()
	{
	union(){
	translate([0,0,28]) cube([25,10,8], center = true);
	cylinder(r=r, h=40);
	hull(){
	w = 25;
	translate([-w/2,-10,10]) cube([w,20,1]);
	translate([-w/2,-20,0]) cube([w,40,1]);
}
	}
	//nut
	cylinder(r=7.9, h=50, $fn=6);
	}
	difference()
	{
	cylinder(r=7.9, h=5);
	cylinder(r=4.2, h=5);
	}

}

module screw()
{
	translate([0,0,-15]) cylinder(r=1.6, h=40);
	cylinder(r=3.6, h=3, $fn = 6);
	//translate([0,0,-15]) cylinder(r=5, h=9);
	translate([-7.2/2,0,0]) cube([7.2,7.2,3]);

}

module screwNest()
{
	difference(){
		translate([-5,-4,0]) cube([10,6,8]);
		translate([0,0,2]) screw();
	}
}