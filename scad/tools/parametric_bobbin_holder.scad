width = 21.3;
height = 35.8;
deepth = 40; //47

rotate([0,0,30])coupler(deepth);
translate([0, 0, deepth/2]) 
difference()
{
	union(){
	cube([width, height, deepth], center =  true);
	translate([0,0,-deepth/2+0.5]) cube([width+8, height+3, 1], center =  true);
	}
for(i = [[1,1,1],[1,-1,1],[-1,-1,1],[-1,1,1]])
	scale(i)	translate([0.5,0.5,-deepth/2]) cube([width/2-1, height/2-1, deepth]);
translate([0, 0, -deepth/2])cylinder(r=7.5, h = deepth);
}
module coupler(h = 40, r = 20/2)
{
	difference()
	{
	union(){
	cylinder(r=r, h=h, , $fn=6);
	}
	//nut
	cylinder(r=7.95, h=h+10, $fn=6);
	}
	difference()
	{
	cylinder(r=7.95, h=5);
	cylinder(r=4.2, h=5);
	}

}
