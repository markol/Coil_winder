## Automated coil winding machine
This repository contains source files for 3D printable, digitally controlled, coil winding machine. Inspired by few designs already available, including [this](http://www.verelec.nl/?page_id=90).

- `controler_app` directory has GUI control panel client app which uses serial port for communication with microcontroller.
- `scad` directory contains printable parts (like gears) designed with OpenSCAD, including extras. 
- `mechanics` directory with FreeCAD sources including project assembly. 

For assembly modification [install](https://freecadweb.org/wiki/How_to_install_additional_workbenches) additional FreeCAD *assembly2* workbench.
Check [this](mechanics/BOM.md) file for parts list.
Go to [this repo](https://gitlab.com/markol/Teathimble_Firmware/tree/coil_winder) (branch *coil\_winder*) for firmware.
As a electronic board use any AVR based from 3D printer or CNC controller (RAMPS, CNC Shield) or make your own, dedicated like [this](.).


![preview](preview.jpg)
